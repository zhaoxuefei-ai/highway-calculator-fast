/**
 * Copyright (c) 2016-2019 人人开源 All rights reserved.
 * <p>
 * https://www.renren.io
 * <p>
 * 版权所有，侵权必究！
 */

package com.wdjt.highwayCalculatorFast.modules.app.controller;


import com.wdjt.highwayCalculatorFast.common.utils.R;
import com.wdjt.highwayCalculatorFast.common.validator.ValidatorUtils;
import com.wdjt.highwayCalculatorFast.modules.app.form.LoginForm;
import com.wdjt.highwayCalculatorFast.modules.app.interceptor.AuthorizationInterceptor;
import com.wdjt.highwayCalculatorFast.modules.app.service.UserService;
import com.wdjt.highwayCalculatorFast.modules.app.utils.JwtUtils;
import com.wdjt.highwayCalculatorFast.modules.sys.service.SysCaptchaService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

/**
 * APP登录授权
 *
 * @author Mark sunlightcs@gmail.com
 */
@RestController
@RequestMapping("/app")
@Api("APP登录接口")
@Slf4j
public class AppLoginController {
    @Autowired
    private UserService userService;
    @Autowired
    private JwtUtils jwtUtils;
    @Autowired
    private SysCaptchaService sysCaptchaService;


    /**
     * 登录
     */
    @PostMapping("login")
    @ApiOperation("登录")
    public R login(@RequestBody LoginForm form) {
        //表单校验
        ValidatorUtils.validateEntity(form);

        boolean captcha = sysCaptchaService.validate(form.getUuid(), form.getCaptcha());
        if (!captcha) {
            return R.error("验证码不正确");
        }


        // 添加用户过期时间到form中
        form.setExpireDate(userService.queryByUsername(form.getUsername()).getExpireTime());
        //用户登录
        long userId = userService.login(form);

        //生成token
        String token = jwtUtils.generateToken(userId);

        Map<String, Object> map = new HashMap<>();
        map.put("token", token);
        map.put("expire", jwtUtils.getExpire());

        return R.ok(map);
    }

    /**
     * 登出
     */
    @PostMapping("logout")
    @ApiOperation("登出")
    public R logout(HttpServletRequest request) {
        Long userId = (Long) request.getAttribute(AuthorizationInterceptor.USER_KEY);
        log.info("userId: " + userId + " has logout");
        return R.ok();
    }

}
