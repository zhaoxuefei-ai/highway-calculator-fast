package com.wdjt.highwayCalculatorFast.modules.app.service.impl;

import cn.hutool.json.JSONUtil;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wdjt.highwayCalculatorFast.common.utils.PageUtils;
import com.wdjt.highwayCalculatorFast.common.utils.Query;

import com.wdjt.highwayCalculatorFast.modules.app.dao.RecordDao;
import com.wdjt.highwayCalculatorFast.modules.app.entity.RecordEntity;
import com.wdjt.highwayCalculatorFast.modules.app.service.RecordService;


@Service("recordService")
public class RecordServiceImpl extends ServiceImpl<RecordDao, RecordEntity> implements RecordService {
    @Autowired
    private RecordDao recordDao;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        Long userId = (Long) params.get("userId");
        String key = (String) params.get("key");
        IPage<RecordEntity> page = this.page(
                new Query<RecordEntity>().getPage(params),
                new QueryWrapper<RecordEntity>()
                        .like(!StringUtils.isBlank(key), "category", key)
                        .eq(userId != null && userId != 0L, "user_id", userId)
//                        .orderByAsc("create_time")
                        .orderByDesc("create_time")
        );

        return new PageUtils(page);
    }

    @Override
    public void deleteBatch(Map<String, Object> params) {
        params.get("ids");

        String a = params.get("ids").toString();
        System.out.println(a.length() + "~~~~~~~~");

        if (a.length() > 2) {
            String substring = a.substring(1, a.length() - 1);

            System.out.println("substring=====>" + substring);

            String[] split = substring.split(",", substring.length());


            Long[] l = new Long[split.length];


            for (int i = 0; i < split.length; i++) {
                System.out.println(split);
                l[i] = Long.parseLong(split[i].trim());
            }

            for (Long aLong : l) {
                System.out.println("long====>" + aLong);
            }

            recordDao.deleteBatch(l);
        } else {
            System.out.println(a);

            Long[] l = new Long[a.length()];

            for (int i = 0; i < a.trim().length(); i++) {
                l[i] = Long.parseLong(a.trim());
            }
            recordDao.deleteBatch(l);
        }
    }

}
