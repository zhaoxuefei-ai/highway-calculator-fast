package com.wdjt.highwayCalculatorFast.modules.app.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import java.util.List;

@Data
public class CalculatorEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 计算器ID
     */
    private Long menuId;

    /**
     * 计算器名称
     */
    private String name;

    /**
     * 计算器URL
     */
    private String url;

}
