package com.wdjt.highwayCalculatorFast.modules.oss.service.impl;

import com.wdjt.highwayCalculatorFast.common.utils.Constant;
import com.wdjt.highwayCalculatorFast.modules.oss.service.FileStoreService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.UUID;

/**
 * 获取服务器地址和图片
 * 返回服务器中的图片地址
 *
 * @author pzt
 */
@Service
public class FileStoreServiceImpl implements FileStoreService {

    @Value("${server.servlet.context-path}")
    public String contextPath;

    @Override
    public String getDownloadPrefix() {
        return "http://" + Constant.ip + ":" + Constant.port + contextPath + "/oss/download/";
    }

    public  String getUploadFolderPath() {
        File directory = new File("");
        return directory.getAbsolutePath() + "/" + uploadFolderName + "/";
    }


    public  ArrayList<String> addImgs(MultipartFile[] photos,String fileDownloadUrlProfix ) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy/MM/dd/");
        String realPath = getUploadFolderPath();
        String format = simpleDateFormat.format(new Date());
        File folder = new File(realPath + format);
        if (!folder.isDirectory()) {
            folder.mkdirs();
        }
        ArrayList<String> strings = new ArrayList<>();
        for (int i = 0; i < photos.length; i++) {
            MultipartFile photo = photos[i];
            String oldName = photo.getOriginalFilename();
            String newName = UUID.randomUUID().toString() +
                    oldName.substring(oldName.lastIndexOf("."), oldName.length());
            String filePath = "";
            try {
                File desk = new File(folder, newName);
                photo.transferTo(desk);
                filePath = fileDownloadUrlProfix + format + newName;
                strings.add(filePath);
            } catch (IOException e) {
                System.out.println(new Date() + e.getMessage());
            }
        }
        return strings;
    }


}
